# Jeager Node Monitoring-Backend's deployment script
Use these scripts to run the backend of Jeager Node Monitoring, developed by Ketitik.

## Status
Under Development

## How to Run
### Edit `.env` to set the basic configuration of InfluxDB.
```bash
INFLUX_USERNAME=actifed
INFLUX_PASSWORD=obat_batuk
INFLUX_HTTP_AUTH=true
INFLUX_DB=atron_monitoring_db
```
### Edit `conf_mongodb/mongo_setup.js` to set the basic configuration of MongoDB.
```javascript
...
db = db.getSiblingDB('atron_monitoring_db');
...
db.createUser({
	user: 'SabinaAtronbekopa',
	pwd: '1koma82meter',
	roles: [{ role: 'readWrite', db:'atron_monitoring_db'}]
})
...
```
### Edit `conf_kafka/server.properties` to set the basic configuration of Kafka Message Broker.
```bash
# Custom Configuration
# Edit below lines as needed.
## Basic Configuration
advertised.host.name=<host-of-message-broker>
advertised.port=9092
log.retention.hours=168
## Auth
listeners=SASL_PLAINTEXT://<host-of-message-broker>:9092
security.inter.broker.protocol=SASL_PLAINTEXT
sasl.mechanism.inter.broker.protocol=PLAIN
sasl.enabled.mechanisms=PLAIN
advertised.listeners=SASL_PLAINTEXT://<host-of-message-broker>:9092
```

### Then, execute to wake the containers up.
```bash
(../atron-trial/) $ ./up.sh
```

To put the containers down, execute
```bash
(../atron-trial/) $ ./down.sh
```

## License
Ketitik's Proprietary.
