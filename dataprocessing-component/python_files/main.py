#!/usr/bin/env python

#pandrev1.1
import pandas as pd
import numpy as np
import time
from influxdb import InfluxDBClient
from sqlalchemy import create_engine
from config import *
from apscheduler.schedulers.background import BackgroundScheduler

"""
Postgre configuration
=============================================
postgresql://SabinaAtronbekopa:1koma82meter@10.62.170.154/atron_monitoring_db
=>    service://user:password@hostname/db
"""
engine = create_engine('postgresql://{}:{}@{}/{}'.format(postgre_user,postgre_password,postgre_hostname,postgre_dbname), echo=False)

"""
InfluxDBClient initiation.
"""
client=InfluxDBClient(influx_hostname,influx_port,influx_user,influx_password,influx_dbname)

"""
Scheduler initiation.
"""
scheduler = BackgroundScheduler()

def occupancy_processing():
    print("Start Processing")
    global_start = time.time()
    """
    InfluxDB's query get result.
    took about 2 minutes to done.
    """
    query_zte = "select LAST(in_bps) AS in_bps, LAST(out_bps) AS out_bps FROM (select (derivative(MAX(ifHCInOctets), 1s) * 8) AS in_bps, (derivative(MAX(ifHCOutOctets), 1s) * 8) AS out_bps from interface where ifDescr=~ /(Eth)/ GROUP BY time(5m), agent_host, hostname, ifDescr Fill(linear)) where time >= now()-12m GROUP BY agent_host, hostname, ifDescr"
    query_huawei = "select LAST(in_bps) AS in_bps, LAST(out_bps) AS out_bps FROM (select (derivative(MAX(ifHCInOctets), 1s) * 8) AS in_bps, (derivative(MAX(ifHCOutOctets), 1s) * 8) AS out_bps from interface where ifName=~ /(GP)/ GROUP BY time(5m), agent_host, hostname, ifName Fill(linear)) where time >= now()-12m GROUP BY agent_host, hostname, ifName"
    query_monitored_node = "select COUNT(data) FROM (SELECT LAST(uptime) as data FROM snmp WHERE time >= now()-15m GROUP BY agent_host)"

    start = time.time()
    res_mon = client.query(query_monitored_node,chunked=False)
    points_mon = res_mon.get_points()
    end = time.time()
    print(" Monitored Node Query Elapsed time : {}".format(end-start))

    start = time.time()
    res_zte = client.query(query_zte,chunked=False)
    points_zte = res_zte.get_points()
    indexs_zte = res_zte.keys()
    end = time.time()
    print(" ZTE Query Elapsed time : {}".format(end-start))

    start = time.time()
    res_huawei = client.query(query_huawei,chunked=False)
    points_huawei = res_huawei.get_points()
    indexs_huawei = res_huawei.keys()
    end = time.time()
    print(" Huawei Query Elapsed time : {}".format(end-start))

    """
    InfluxDB's keys or tags/fields result parsing into fields list.
    """
    fields_huawei = []
    for i in range(len(indexs_huawei)):
        res = (indexs_huawei[i][1]['agent_host']),(indexs_huawei[i][1]['ifName']),(indexs_huawei[i][1]['hostname'])
        fields_huawei.append(res)

    fields_zte = []
    for i in range(len(indexs_zte)):
        res = (indexs_zte[i][1]['agent_host']),(indexs_zte[i][1]['ifDescr']),(indexs_zte[i][1]['hostname'])
        fields_zte.append(res)

    """
    InfluxDB keys and points convertion into DataFrame.
    """
    df_influx_keys_zte = pd.DataFrame(fields_zte,columns=['agent_host','port_uplink','hostname'])
    df_influx_keys_huawei = pd.DataFrame(fields_huawei,columns=['agent_host','port_uplink','hostname'])
    df_influx_points_zte = pd.DataFrame(points_zte)
    df_influx_points_huawei = pd.DataFrame(points_huawei)
    """
    InfluxDB keys and points merge.
    """
    df_influx_h = pd.concat([df_influx_keys_huawei, df_influx_points_huawei], axis=1)
    df_influx_z = pd.concat([df_influx_keys_zte, df_influx_points_zte], axis=1)
    """
    Merged influxDB result and drop no traffic ports
    no traffic ports are saved on no_traffic dataframe (not used for now)
    """
    df_influx = pd.concat([df_influx_h,df_influx_z])
    df_influx = df_influx.reset_index()
    #no_traffic = df_influx[(df_influx['in_bps']==0)&(df_influx['out_bps']==0)]
    df_influx = df_influx.drop(df_influx[(df_influx['in_bps']==0) & (df_influx['out_bps']==0)].index).reset_index()
    df_influx = df_influx.drop(columns=['index','time'])
    df_influx_max = df_influx[df_influx.groupby('agent_host')['in_bps'].transform(max) == df_influx['in_bps']]
    df_influx_max = df_influx_max.rename(index=str, columns={"in_bps": "max_in_bps"})
    df_influx_done = df_influx_max.drop(columns=['level_0','out_bps'])
    df_mon = pd.DataFrame(points_mon)
    df_mon = df_mon.rename(index=str, columns={"count":"up"})
    df_mon['monitored'] = 2603
    df_mon['down'] = df_mon['monitored']-df_mon['up']
    '''
    Read Bandwidth Data from postgre
    '''
    pq = 'SELECT * FROM bw_data'
    bw_data = pd.read_sql(sql=pq,con=engine)

    pq_daily_occ = 'SELECT site_id,daily_occ FROM daily_occ_current'
    daily_occ = pd.read_sql(sql=pq_daily_occ,con=engine)

    pq_weekly_occ = 'SELECT site_id,weekly_occ FROM weekly_occ_current'
    weekly_occ = pd.read_sql(sql=pq_weekly_occ,con=engine)
    '''
    Drop any duplicates siteid
    '''
    bw_data = bw_data.drop_duplicates(subset='site_id',keep='first')

    '''
    Convert bw_tps from Mb to bit
    '''
    bw_data['bw_tps'] = pd.to_numeric(bw_data['bw_tps'])
    bw_data['bw_tps_bps'] = pd.to_numeric(bw_data['bw_tps']*1000000)

    '''
    Merge bw_data and influx data, occupancy calculation and group by each maximum occupancy per site id.
    '''
    occupancy = pd.merge(bw_data,df_influx_done,on='agent_host')
    occupancy['current_occ'] = occupancy['max_in_bps']*100/occupancy['bw_tps_bps']
    occupancy = pd.merge(occupancy,daily_occ,on='site_id')
    occupancy = pd.merge(occupancy,weekly_occ,on='site_id')
    '''
    Drop unnecessary columns and add time processing columns.
    '''
    occupancy_to_db = occupancy.drop(columns=['bw_tps_bps','max_in_bps'])
    occupancy_to_db['time'] = pd.Timestamp.now()
    occupancy_to_db = occupancy_to_db.rename(index=str, columns={"bw_tps":"bandwidth"})
    occupancy_to_db = occupancy_to_db.round({'current_occ':0})
    occupancy_to_db['current_occ'] = pd.to_numeric(occupancy_to_db['current_occ'],downcast='integer')
    occupancy_to_db['status'] = 'UP'
    occupancy_to_db['monthly_occ'] = 0
    occupancy_to_db['yearly_occ'] = 0
    ridar_occ = occupancy_to_db[occupancy_to_db['witel']=='RIDAR']
    medan_occ = occupancy_to_db[occupancy_to_db['witel']=='MEDAN']
    sumsel_occ = occupancy_to_db[occupancy_to_db['witel']=='SUMSEL']

    counter = pd.DataFrame(columns=['witel','lessthan50','from50to70','morethan70','total'])
    counter.loc[0] = 'ridar',len(ridar_occ[ridar_occ['current_occ']<50]),len(ridar_occ[(ridar_occ['current_occ']>50)&(ridar_occ['current_occ']<70)]),len(ridar_occ[ridar_occ['current_occ']>70]),len(ridar_occ[ridar_occ['current_occ']<50])+len(ridar_occ[(ridar_occ['current_occ']>50)&(ridar_occ['current_occ']<70)])+len(ridar_occ[ridar_occ['current_occ']>70])
    counter.loc[1] = 'medan',len(medan_occ[medan_occ['current_occ']<50]),len(medan_occ[(medan_occ['current_occ']>50)&(medan_occ['current_occ']<70)]),len(medan_occ[medan_occ['current_occ']>70]),len(medan_occ[medan_occ['current_occ']<50])+len(medan_occ[(medan_occ['current_occ']>50)&(medan_occ['current_occ']<70)])+len(medan_occ[medan_occ['current_occ']>70])
    counter.loc[2] = 'sumsel',len(sumsel_occ[sumsel_occ['current_occ']<50]),len(sumsel_occ[(sumsel_occ['current_occ']>50)&(sumsel_occ['current_occ']<70)]),len(sumsel_occ[sumsel_occ['current_occ']>70]),len(sumsel_occ[sumsel_occ['current_occ']<50])+len(sumsel_occ[(sumsel_occ['current_occ']>50)&(sumsel_occ['current_occ']<70)])+len(sumsel_occ[sumsel_occ['current_occ']>70])

    '''
    Insert occupancy data to postgre.
    '''
    start = time.time()
    occupancy_to_db.to_sql('all_occ',if_exists='append',con=engine)
    ridar_occ.to_sql('ridar_occ',if_exists='replace',con=engine)
    medan_occ.to_sql('medan_occ',if_exists='replace',con=engine)
    sumsel_occ.to_sql('sumsel_occ',if_exists='replace',con=engine)
    counter.to_sql('counter_occ',if_exists='replace',con=engine)
    df_mon.to_sql('node_status',if_exists='replace',con=engine)
    end = time.time()
    print(" To SQL Elapsed time : {}".format(end-start))
    global_stop = time.time()
    print("Processing done, result sent to postgresql")
    print("Elapsed time: ",global_stop-global_start)

def main():
    job = scheduler.add_job(occupancy_processing,trigger='cron', minute="*/5")
    scheduler.start()
    while True:
        time.sleep(1)

if __name__ == "__main__":
    main()
