from flask import Flask
from marshmallow import Schema, fields, pre_load, validate
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

ma = Marshmallow()
db = SQLAlchemy()


class Topridar(db.Model):
    __tablename__ = 'ridar_occ'
    index = db.Column(db.Integer, primary_key=True)
    site_id = db.Column(db.String)
    site_name = db.Column(db.String)
    witel = db.Column(db.String)
    agent_host = db.Column(db.String)
    bandwidth = db.Column(db.Integer)
    port_uplink = db.Column(db.String)
    status = db.Column(db.String)
    hostname = db.Column(db.String)
    current_occ = db.Column(db.Float)
    daily_occ = db.Column(db.Integer)
    weekly_occ = db.Column(db.Integer)
    monthly_occ = db.Column(db.Integer)
    yearly_occ = db.Column(db.Integer)
    time = db.Column(db.TIMESTAMP)

    def __init__(self, name):
        self.name = name

class TopridarSchema(ma.Schema):
    index = fields.Integer()
    site_id = fields.String()
    site_name = fields.String()
    witel = fields.String()
    agent_host = fields.String()
    bandwidth = fields.Integer()
    port_uplink = fields.String()
    status = fields.String()
    hostname = fields.String()
    current_occ = fields.Float()
    daily_occ = fields.Integer()
    weekly_occ = fields.Integer()
    monthly_occ = fields.Integer()
    yearly_occ = fields.Integer()
    time = fields.DateTime()

class Topsumsel(db.Model):
    __tablename__ = 'sumsel_occ'
    index = db.Column(db.Integer, primary_key=True)
    site_id = db.Column(db.String)
    site_name = db.Column(db.String)
    witel = db.Column(db.String)
    agent_host = db.Column(db.String)
    bandwidth = db.Column(db.Integer)
    port_uplink = db.Column(db.String)
    status = db.Column(db.String)
    hostname = db.Column(db.String)
    current_occ = db.Column(db.Float)
    daily_occ = db.Column(db.Integer)
    weekly_occ = db.Column(db.Integer)
    monthly_occ = db.Column(db.Integer)
    yearly_occ = db.Column(db.Integer)
    time = db.Column(db.TIMESTAMP)

    def __init__(self, name):
        self.name = name

class TopsumselSchema(ma.Schema):
    index = fields.Integer()
    site_id = fields.String()
    site_name = fields.String()
    witel = fields.String()
    agent_host = fields.String()
    bandwidth = fields.Integer()
    port_uplink = fields.String()
    status = fields.String()
    hostname = fields.String()
    current_occ = fields.Float()
    daily_occ = fields.Integer()
    weekly_occ = fields.Integer()
    monthly_occ = fields.Integer()
    yearly_occ = fields.Integer()
    time = fields.DateTime()

class Topmedan(db.Model):
    __tablename__ = 'medan_occ'
    index = db.Column(db.Integer, primary_key=True)
    site_id = db.Column(db.String)
    site_name = db.Column(db.String)
    witel = db.Column(db.String)
    agent_host = db.Column(db.String)
    bandwidth = db.Column(db.Integer)
    port_uplink = db.Column(db.String)
    status = db.Column(db.String)
    hostname = db.Column(db.String)
    current_occ = db.Column(db.Float)
    daily_occ = db.Column(db.Integer)
    weekly_occ = db.Column(db.Integer)
    monthly_occ = db.Column(db.Integer)
    yearly_occ = db.Column(db.Integer)
    time = db.Column(db.TIMESTAMP)

    def __init__(self, name):
        self.name = name

class TopmedanSchema(ma.Schema):
    index = fields.Integer()
    site_id = fields.String()
    site_name = fields.String()
    witel = fields.String()
    agent_host = fields.String()
    bandwidth = fields.Integer()
    port_uplink = fields.String()
    status = fields.String()
    hostname = fields.String()
    current_occ = fields.Float()
    daily_occ = fields.Integer()
    weekly_occ = fields.Integer()
    monthly_occ = fields.Integer()
    yearly_occ = fields.Integer()
    time = fields.DateTime()

class Witelocc(db.Model):
    __tablename__ = 'counter_occ'
    index = db.Column(db.Integer, primary_key=True)
    witel = db.Column(db.String)
    lessthan50 = db.Column(db.Integer)
    from50to70 = db.Column(db.Integer)
    morethan70 = db.Column(db.Integer)
    total = db.Column(db.Integer)

    def __init__(self, name):
        self.name = name

class WiteloccSchema(ma.Schema):
    index = fields.Integer()
    witel = fields.String()
    lessthan50 = fields.Integer()
    from50to70 = fields.Integer()
    morethan70 = fields.Integer()
    total = fields.Integer()


class Nodestatus(db.Model):
    __tablename__ = 'node_status'
    up = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.TIMESTAMP)
    monitored = db.Column(db.Integer)
    down = db.Column(db.Integer)

    def __init__(self, name):
        self.name = name

class NodestatusSchema(ma.Schema):
    up = fields.Integer()
    time = fields.DateTime()
    monitored = fields.Integer()
    down = fields.Integer()


class Bandwidth(db.Model):
    __tablename__ = 'bw_data'
    site_id = db.Column(db.String, primary_key=True)
    site_name = db.Column(db.String)
    witel = db.Column(db.String)
    agent_host = db.Column(db.String)
    bw_tps = db.Column(db.String)

    def __init__(self, site_id, site_name, witel, agent_host, bw_tps):
        self.site_id = site_id
        self.site_name = site_name
        self.witel = witel
        self.agent_host = agent_host
        self.bw_tps = bw_tps

class BandwidthSchema(ma.Schema):
    site_id = fields.String()
    site_name = fields.String()
    witel = fields.String()
    agent_host = fields.String()
    bw_tps = fields.String()
