from flask import request
from flask_restful import Resource
from Model import db, Topridar, TopridarSchema, Topmedan, TopmedanSchema, Topsumsel, TopsumselSchema

topridar_schema = TopridarSchema(only=('site_id', 'bandwidth','current_occ'),many=True)
topmedan_schema = TopmedanSchema(only=('site_id', 'bandwidth','current_occ'),many=True)
topsumsel_schema = TopsumselSchema(only=('site_id', 'bandwidth','current_occ'),many=True)

class TopoccResource(Resource):
    def get(self):
        ridar_occ = Topridar.query.order_by(Topridar.current_occ.desc()).limit(50).all()
        ridar_occ = topridar_schema.dump(ridar_occ).data
        ridar_result = {'witel':'RIDAR','contents': ridar_occ}

        medan_occ = Topmedan.query.order_by(Topmedan.current_occ.desc()).limit(50).all()
        medan_occ = topmedan_schema.dump(medan_occ).data
        medan_result = {'witel':'MEDAN','contents': medan_occ}

        sumsel_occ = Topsumsel.query.order_by(Topsumsel.current_occ.desc()).limit(50).all()
        sumsel_occ = topsumsel_schema.dump(sumsel_occ).data
        sumsel_result = {'witel':'SUMSEL','contents': sumsel_occ}
        # print(occupancy)
        return {"data":[ridar_result,medan_result,sumsel_result]}, 200
