from flask import request
from flask_restful import Resource
from Model import db, Nodestatus, NodestatusSchema

nodestatus_schema = NodestatusSchema(only=('up', 'down'),many=True)


class NodestatusResource(Resource):
    def get(self):
        node_stat = Nodestatus.query.all()
        node_stat = nodestatus_schema.dump(node_stat).data

        return {"data": node_stat}, 200
